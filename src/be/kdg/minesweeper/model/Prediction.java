package be.kdg.minesweeper.model;

public enum Prediction {
    NONE, MAYBE, FLAG
}
