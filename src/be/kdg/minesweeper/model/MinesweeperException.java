package be.kdg.minesweeper.model;

public class MinesweeperException extends RuntimeException {
    public MinesweeperException(String msg) {
        super(msg);
    }
}
