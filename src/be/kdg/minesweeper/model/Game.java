package be.kdg.minesweeper.model;

import be.kdg.minesweeper.model.scores.ScoreManager;
import be.kdg.minesweeper.model.scores.Score;

import java.util.List;

/**
 * This is the "entry point" model class that delegates
 * the game logic. It contains the grid, the score, some
 * timing information, and the game state.
 * Use this class to make moves, check if victory conditions
 * are met, and to gain access to the grid.
 * @author Lars Willemsens
 * @version 1.0
 */
public class Game {
    private final Grid grid;
    private final ScoreManager scoreManager;
    private boolean isStarted;
    private boolean isFinished;
    private Score score;
    private int seconds;

    /**
     * Creates a new game with a specified difficulty.
     * @param difficulty The difficulty for the new game.
     */
    public Game(Difficulty difficulty) {
        this.grid = new Grid(difficulty);
        this.scoreManager = new ScoreManager();
        this.score = null;
        this.isStarted = false;
        this.isFinished = false;
        this.seconds = 0;
    }

    public Difficulty getDifficulty() {
        return grid.getDifficulty();
    }

    private Grid getGrid() {
        return this.grid;
    }

    private void checkFinished() {
        if (this.getGrid().getRowCount() * this.grid.getColumnCount() - this.grid.getDifficulty().getBombs() == this.grid.getNumberOfRevealedCells()) {
            this.isFinished = true;
            this.score = new Score(this.seconds);
            this.scoreManager.gamePlayed(this.getDifficulty(), this.score);
        }
    }

    /**
     * Play a single move. A certain specified cell will be revealed.
     * In case a revealed cell has no neighbouring bombs, then all
     * neighbouring cells will be revealed as well (possibly revealing
     * even more cells).
     * @param row The row of the cell to reveal.
     * @param column The column of the cell to reveal.
     * @return All the cells that are revealed to the player.
     * @throws MinesweeperException If the game is already finished.
     */
    public List<Cell> play(int row, int column) {
        if (!this.isFinished) {
            this.isStarted = true;
            Cell targetCell = getGrid().getCell(row, column);
            if (targetCell.hasBomb()) {
                this.isFinished = true;
                this.scoreManager.gamePlayed(this.getDifficulty(), null);
                return this.grid.getAllBombs();
            } else {
                List<Cell> cellsToReveal = this.grid.getCellsToReveal(row, column);
                checkFinished();
                return cellsToReveal;
            }
        } else {
            throw new MinesweeperException("The game is already finished.");
        }
    }

    public Cell getCell(int row, int column) {
        return getGrid().getCell(row, column);
    }

    public void changePrediction(int row, int column) {
        if (!this.isFinished) {
            Cell targetCell = getGrid().getCell(row, column);
            if (!targetCell.isRevealed()) {
                Prediction newPrediction;
                if (targetCell.getPrediction() == Prediction.NONE) {
                    newPrediction = Prediction.FLAG;
                } else if (targetCell.getPrediction() == Prediction.FLAG) {
                    newPrediction = Prediction.MAYBE;
                } else {
                    newPrediction = Prediction.NONE;
                }
                targetCell.setPrediction(newPrediction);
            } else {
                throw new MinesweeperException("Can't make a prediction for a revealed cell.");
            }
        } else {
            throw new MinesweeperException("The game is already finished.");
        }
    }

    public boolean isRevealed(int row, int column) {
        return this.getGrid().getCell(row, column).isRevealed();
    }

    public boolean isStarted() {
        return isStarted;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public boolean isWon() {
        return this.score != null;
    }

    public int getNeighbouringBombs(int row, int column) {
        return this.getGrid().getNeighbouringBombs(row, column);
    }

    public void increaseTimer() {
        this.seconds++;
    }

    public int getSeconds() {
        return seconds;
    }

    public ScoreManager getScoreManager() {
        return scoreManager;
    }
}
