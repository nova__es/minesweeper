package be.kdg.minesweeper.view.main;

import be.kdg.minesweeper.model.Difficulty;
import be.kdg.minesweeper.view.Tile;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

public class MainView extends BorderPane {
    private ImageView[][] cells;
    private GridPane gridPane;
    private HBox bottomBar;
    private MenuItem menuItemRestart;
    private MenuItem menuItemQuit;
    private RadioMenuItem menuItemEasy;
    private RadioMenuItem menuItemMedium;
    private RadioMenuItem menuItemHard;
    private MenuItem menuItemAbout;
    private Label timer;

    public MainView(Difficulty difficulty) {
        this.initialiseNodes();
        this.layoutNodes();

        setUpGame(difficulty);
    }

    void setUpGame(Difficulty difficulty) {
        this.initialiseGridPane(difficulty);
        this.layoutGridPane();
        this.setTimer(0);
    }

    private void initialiseNodes() {
        this.menuItemRestart = new MenuItem("Restart");
        this.menuItemQuit = new MenuItem("Quit");

        ToggleGroup difficultyGroup = new ToggleGroup();
        this.menuItemEasy = new RadioMenuItem("Easy");
        this.menuItemEasy.setToggleGroup(difficultyGroup);
        this.menuItemMedium = new RadioMenuItem("Intermediate");
        this.menuItemMedium.setToggleGroup(difficultyGroup);
        this.menuItemHard = new RadioMenuItem("Expert");
        this.menuItemHard.setToggleGroup(difficultyGroup);

        this.menuItemAbout = new MenuItem("About");

        this.bottomBar = new HBox();
        this.timer = new Label("0");
    }

    private void initialiseGridPane(Difficulty difficulty) {
        this.gridPane = new GridPane();

        this.cells = new ImageView
                [difficulty.getWidth()]
                [difficulty.getHeight()];

        for (int i = 0; i < this.cells.length; i++) {
            for (int j = 0; j < this.cells[i].length; j++) {
                this.cells[i][j] = new ImageView(Tile.UNKNOWN.getImage());
            }
        }

        this.menuItemEasy.setSelected(difficulty == Difficulty.EASY);
        this.menuItemMedium.setSelected(difficulty == Difficulty.MEDIUM);
        this.menuItemHard.setSelected(difficulty == Difficulty.HARD);
    }

    private void layoutNodes() {
        MenuBar menuBar = new MenuBar();

        Menu fileMenu = new Menu("File");
        fileMenu.getItems().addAll(menuItemRestart, menuItemQuit);

        Menu difficultyMenu = new Menu("Difficulty");
        difficultyMenu.getItems().addAll(menuItemEasy, menuItemMedium, menuItemHard);

        Menu helpMenu = new Menu("Help");
        helpMenu.getItems().add(menuItemAbout);

        menuBar.getMenus().add(fileMenu);
        menuBar.getMenus().add(difficultyMenu);
        menuBar.getMenus().add(helpMenu);

        this.setTop(menuBar);

        this.bottomBar.setAlignment(Pos.CENTER_RIGHT);
        this.bottomBar.getChildren().add(this.timer);
        this.bottomBar.setPadding(new Insets(0.0, 5.0, 5.0, 0.0));

        this.setBottom(this.bottomBar);
    }

    private void layoutGridPane() {
        //this.gridPane.setGridLinesVisible(true);
        this.gridPane.setPadding(new Insets(5.0));
        for (int i = 0; i < this.cells.length; i++) {
            for (int j = 0; j < this.cells[i].length; j++) {
                this.gridPane.add(this.cells[i][j], i, j);
                GridPane.setMargin(this.cells[i][j], new Insets(1.0));
            }
        }

        this.setCenter(this.gridPane);
    }

    ImageView[][] getCells() {
        return this.cells;
    }

    MenuItem getMenuItemRestart() {
        return menuItemRestart;
    }

    MenuItem getMenuItemQuit() {
        return menuItemQuit;
    }

    RadioMenuItem getMenuItemEasy() {
        return menuItemEasy;
    }

    RadioMenuItem getMenuItemMedium() {
        return menuItemMedium;
    }

    RadioMenuItem getMenuItemHard() {
        return menuItemHard;
    }

    MenuItem getMenuItemAbout() {
        return menuItemAbout;
    }

    void setTimer(int seconds) {
        this.timer.setText(String.valueOf(seconds));
    }
}
