package be.kdg.minesweeper.view.gameover;

import be.kdg.minesweeper.model.Game;
import be.kdg.minesweeper.model.scores.Score;
import javafx.stage.Stage;

public class GameOverPresenter {
    private final Game model;
    private final GameOverView view;

    private boolean restart;

    public GameOverPresenter(Game model, GameOverView view) {
        this.model = model;
        this.view = view;

        this.addEventHandlers();
        this.updateView();
    }

    private void addEventHandlers() {
        this.view.getExitButton().setOnAction(event -> {
            this.restart = false;
            ((Stage) this.view.getScene().getWindow()).close();
        });

        this.view.getPlayAgainButton().setOnAction(playAgainEvent -> {
            this.restart = true;
            ((Stage) this.view.getScene().getWindow()).close();
        });
    }

    public boolean shouldRestartGame() {
        return restart;
    }

    private void updateView() {
        if (this.model.isWon()) {
            this.view.getCongratulationsLabel().setText("Congratulations, you won the game!");
        } else {
            this.view.getCongratulationsLabel().setText("You lost, better luck next time!");
        }

        this.view.getTimeLabel().setText("Time: " + this.model.getSeconds() + " seconds");

        Score score = this.model.getScoreManager().getHighScore(this.model.getDifficulty());
        String bestTimeText;
        String bestDateText;
        if (score != null) {
            bestTimeText = "Best time: " + score.getSeconds() + " seconds";
            bestDateText = "Date: " + score.getDate();
        } else {
            bestTimeText = "";
            bestDateText = "";
        }

        this.view.getBestTimeLabel().setText(bestTimeText);
        this.view.getDateLabel().setText(bestDateText);
        this.view.getGamesPlayedLabel().setText("Games played: " + this.model.getScoreManager().getGamesPlayed());
        this.view.getGamesWonLabel().setText("Games won: " + this.model.getScoreManager().getGamesWon());
        this.view.getPercentageLabel().setText("Percentage: " + Math.round(this.model.getScoreManager().getGamesWon() * 100.0 / this.model.getScoreManager().getGamesPlayed()));
    }
}
